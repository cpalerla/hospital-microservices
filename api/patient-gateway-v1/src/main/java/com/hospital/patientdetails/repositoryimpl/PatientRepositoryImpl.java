package com.hospital.patientdetails.repositoryimpl;

import com.hospital.patientdetails.model.Patient;
import com.hospital.patientdetails.repositoy.PatientRepository;
import java.security.KeyStore.PrivateKeyEntry;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.stereotype.Repository;

@Repository
public class PatientRepositoryImpl implements PatientRepository {

  @Autowired
  private MongoTemplate mongoTemplate;

  @Override
  public void savePatient(Patient patient) {
    mongoTemplate.save(patient,"patient");
  }
}
