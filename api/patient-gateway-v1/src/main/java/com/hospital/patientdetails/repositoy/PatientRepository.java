package com.hospital.patientdetails.repositoy;

import com.hospital.patientdetails.model.Patient;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

public interface PatientRepository {

  void savePatient(Patient patient);
}
