package com.hospital.patientdetails.serviceimpl;

import com.hospital.patientdetails.model.Patient;
import com.hospital.patientdetails.repositoy.PatientRepository;
import com.hospital.patientdetails.service.PatientService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Service
public class PatientServiceImpl implements PatientService {

  @Autowired
  PatientRepository patientRepository;

  @Override
  public ResponseEntity savePatient(Patient patient) {
    patientRepository.savePatient(patient);
    return new ResponseEntity(HttpStatus.CREATED);
  }
}
