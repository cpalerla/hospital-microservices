package com.hospital.patientdetails.model;

import java.time.LocalDate;
import java.util.UUID;
import lombok.Getter;
import lombok.Setter;
import org.springframework.data.mongodb.core.mapping.Document;

@Getter
@Setter
@Document(collection = "patient")
public class Patient {

  private UUID patientId;
  private String firstName;
  private String lastName;
  private LocalDate dateOfBirth;
  private String zipCode;
  private String email;
  private String phoneNumber;
  private String externalId;

  private Address address;
  private Organization organization;

}
