package com.hospital.patientdetails.model;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class Address {

	private String streetName;
	
	private String city;
	
	private String country;
	
	private String phoneNumber;
}
