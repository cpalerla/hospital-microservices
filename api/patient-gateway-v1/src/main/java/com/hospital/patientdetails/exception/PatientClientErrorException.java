package com.hospital.patientdetails.exception;

import org.springframework.web.client.HttpClientErrorException;

public class PatientClientErrorException extends Exception {

	private static final long serialVersionUID = 1L;

	public PatientClientErrorException(String mesage,HttpClientErrorException ex) {
		super(mesage, ex);
	}
}
