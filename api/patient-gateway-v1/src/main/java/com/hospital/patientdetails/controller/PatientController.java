package com.hospital.patientdetails.controller;

import com.hospital.patientdetails.model.Patient;
import com.hospital.patientdetails.service.PatientService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/patient")
public class PatientController {

  @Autowired
  private PatientService patientService;

  @PostMapping("/savePatient")
  public ResponseEntity savePatient(@RequestBody  Patient patient){
    return patientService.savePatient(patient);
  }

  @GetMapping("/")
  public String getMessage() {
    return "wel come to string boot";
  }

  @GetMapping("/getMessage/{message}")
  public String getMessage(@PathVariable String message){
    return message+" " +"chandrashekar";
  }
}
