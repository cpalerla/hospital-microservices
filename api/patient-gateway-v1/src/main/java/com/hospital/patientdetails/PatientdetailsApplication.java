package com.hospital.patientdetails;

import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@OpenAPIDefinition
public class PatientdetailsApplication {

  public static void main(String[] args) {
    SpringApplication.run(PatientdetailsApplication.class, args);
  }

}
