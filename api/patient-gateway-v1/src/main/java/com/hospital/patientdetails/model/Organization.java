package com.hospital.patientdetails.model;

import java.util.UUID;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class Organization {
	
	private UUID orgId;
	
	private String orgName;
	
	private Address orgAddress;

}
