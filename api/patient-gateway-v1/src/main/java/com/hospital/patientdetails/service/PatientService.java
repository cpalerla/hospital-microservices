package com.hospital.patientdetails.service;

import com.hospital.patientdetails.model.Patient;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;


public interface PatientService {

  ResponseEntity savePatient(Patient patient);
}
